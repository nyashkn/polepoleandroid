package connect;

/**
 *
 * Created by Nyash on 10/25/15.
 *
 **/
public class Config {
    // server URL configuration
//    http://polepole.192.168.0.33.xip.io/
//    public static final String URL_REQUEST_SMS = "https://paypolepole.com/api/user/sign_up";
//    public static final String URL_VERIFY_OTP = "https://paypolepole.com/api/user/verify_mobile_number";

    //Dev
    public static final String URL_REQUEST_SMS = "http://bellenaserian.pagekite.me/api/v1/users/sign_up";
    public static final String URL_VERIFY_OTP = "http://bellenaserian.pagekite.me/api/v1/users/verify_mobile_number";
    public static final String URL_NOTIFY_UPLOAD_COMPLETE = "http://bellenaserian.pagekite.me/api/v1/users/credit_data_upload";

    // SMS provider identification
    // It should match with your SMS gateway origin
    // You can use  MSGIND, TESTER and ALERTS as sender ID
    // If you want custom sender Id, approve MSG91 to get one
    public static final String SMS_ORIGIN = "VIPEPEO";

    // special character to prefix the otp. Make sure this character appears only once in the sms
    public static final String OTP_DELIMITER = ":";
}
