package receiver;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Created by kenneth on 3/31/16.
 */
public class GCMIntent extends IntentService {

    private static final String TAG = "GcmIntentService";
    public GCMIntent() {
        super("GCMIntent");
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);

        try {
            if (!extras.isEmpty()) {
                if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                    Log.d(TAG, "messageType: " + messageType + ",body:" + extras.toString());
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                    Log.d(TAG, "messageType: " + messageType + ",body:" + extras.toString());
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                    //do something with message here e.g. create notification


                    //make sure json data values are set on server response. Then you can retrieve them like so...
                    Log.d(TAG, extras.get("message").toString());
                    Log.d(TAG, extras.get("title").toString());

                }
            }
            GCMReceiver.completeWakefulIntent(intent);
        }
        catch(Exception r){
            r.printStackTrace();
        }

    }
}
