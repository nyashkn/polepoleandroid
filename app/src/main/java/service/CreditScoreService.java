package service;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import android.provider.Settings.System;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import connect.Config;
import connect.DeviceName;
import helpers.PrefManager;
import polepole.pay.apps.polepole.PoleApplication;

;


public class CreditScoreService extends IntentService {
    private static String TAG = CreditScoreService.class.getSimpleName();

    public CreditScoreService() {
        super(CreditScoreService.class.getSimpleName());
    }

    PrefManager pref;

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Firebase.setAndroidContext(getApplicationContext());
            pref = new PrefManager(getApplicationContext());
            Firebase userFirebaseRef = new Firebase("https://intense-torch-6084.firebaseio.com/").child("users").child(pref.getMobileNumber());
            userFirebaseRef.authWithCustomToken("ytMYMLCwCdYsdzBFIvqoY5eLVESPNNDRUQqNdkCW", authResultHandler);
            addUserMessagesDataToFirebase(userFirebaseRef);
            addDeviceNameToFirebase(userFirebaseRef);
            addCallLogsToFirebase(userFirebaseRef);
            addCellIMEINumber(userFirebaseRef);
            notifyPolePoleServerDataUploaded(pref.getMobileNumber());
            pref.setCreditScoreUploadingStatus(true);

        }
    }

    Firebase.AuthResultHandler authResultHandler = new Firebase.AuthResultHandler() {
        @Override
        public void onAuthenticated(AuthData authData) {
            Toast.makeText(getApplicationContext(), "Authentication Successful", Toast.LENGTH_SHORT);
        }

        @Override
        public void onAuthenticationError(FirebaseError firebaseError) {
            Toast.makeText(getApplicationContext(), "Authentication Unsuccessful", Toast.LENGTH_SHORT);
        }
    };

    private boolean addDeviceNameToFirebase(Firebase userFirebaseRef) {
        String deviceName = DeviceName.getDeviceName();
        Firebase deviceTypeFirebase = userFirebaseRef.child("device");
        deviceTypeFirebase.setValue(deviceName);
        return true;
    }

    private boolean addCallLogsToFirebase(Firebase userFirebaseRef) {
        Uri allCalls = Uri.parse("content://call_log/calls");
        ContentResolver cr = getContentResolver();
        String[] reqCols = new String[]{CallLog.Calls.NUMBER, CallLog.Calls.CACHED_NAME, CallLog.Calls.DURATION, CallLog.Calls.TYPE, CallLog.Calls.DATE};
        Cursor c = cr.query(allCalls, reqCols, null, null, null);
        c.moveToFirst();
        Firebase callsFirebase = userFirebaseRef.child("calls");
        while (c.moveToNext()) {
            Map<String, String> call = new HashMap<String, String>();
            call.put("Number", c.getString(0));
            call.put("Name", c.getString(1));
            call.put("Duration", c.getString(2));
            String[] callTypes = {"Incoming", "Outgoing", "Missed"};
            if (c.getInt(3) > 2) {
                call.put("Type", String.valueOf(c.getInt(3)));
            } else {
                call.put("Type", callTypes[c.getInt(3)]);
            }

            call.put("Date", c.getString(4));
            callsFirebase.child(c.getString(4)).setValue(call);
        }
        c.close();
        return true;
    }

    private boolean addUserMessagesDataToFirebase(Firebase userFirebase) {
        Uri inboxURI = Uri.parse("content://sms/inbox");
        String[] reqCols = new String[]{"_id", "address", "body", "person", "type", "service_center", "date"};
        ContentResolver cr = getContentResolver();
        Firebase messageFirebase = userFirebase.child("messages");
        Cursor c = cr.query(inboxURI, reqCols, null, null, null);
        c.moveToFirst();
        while (c.moveToNext()) {

            boolean patternMatch = Pattern.matches("[+|\\d]\\d{7,14}", c.getString(1));
            if (patternMatch == false) {
                Map<String, String> message = new HashMap<String, String>();
                message.put("ID", String.valueOf(c.getInt(0)));
                message.put("Address", c.getString(1));
                message.put("Body", c.getString(2));
                message.put("Person", c.getString(3));
                message.put("Date", c.getString(6));
                messageFirebase.child(c.getString(6) + String.valueOf(c.getInt(0))).setValue(message);
            }
        }
        c.close();
        return true;
    }
    private boolean addCellIMEINumber(Firebase userFirebase){
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String imeiDeviceID = telephonyManager.getDeviceId();
        String androidID = System.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Firebase deviceTypeFirebase = userFirebase.child("imei");
        Firebase androidIDFirebase = userFirebase.child("android_id");
        deviceTypeFirebase.setValue(imeiDeviceID);
        androidIDFirebase.setValue(androidID);
        return true;
    }
    private void notifyPolePoleServerDataUploaded(final String mobile) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_NOTIFY_UPLOAD_COMPLETE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {

                    JSONObject responseObj = new JSONObject(response);

                    // Parsing json object response
                    // response will be a json object
                    boolean error = responseObj.getBoolean("error");
                    String message = responseObj.getString("message");

                    if (!error) {

                        Toast.makeText(getApplicationContext(), "Successful uploading of credit profile", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getApplicationContext(), "Successful uploading of credit profile", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile", mobile);
                return params;
            }

        };

        // Adding request to request queue
        PoleApplication.getInstance().addToRequestQueue(strReq);
    }
}