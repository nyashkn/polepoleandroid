package polepole.pay.apps.polepole;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by kenneth on 12/13/15.
 */
public class FragmentInvite extends Fragment implements View.OnClickListener{

    Button btnSMS;

    InviteAdapter listAdapter;
    ListView myList;

    private ArrayList<CustomInvite> fetch = new ArrayList<CustomInvite>();


    public FragmentInvite() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_invite, container, false);

        btnSMS = (Button)v.findViewById(R.id.btnSMS);
        btnSMS.setOnClickListener(this);

        //The listview of contacts
        myList = (ListView) v.findViewById(R.id.listPeople);
        // get the listview
        //expListView = (ExpandableListView)rootView.findViewById(R.id.lvExp);

        // preparing list data
        prepareListData();
        System.out.println("Prepare month has been run");

        listAdapter = new InviteAdapter(getActivity(), R.id.listPeople, fetch);

        // setting list adapter
        myList.setAdapter(listAdapter);

        return v;
    }

    @Override
    public void onClick(View v) {
        //add code to launch SMS intent here
    }

    public void prepareListData(){
        //Add your data here in a loop or whatever. I will use dummy data
        for(int a = 0; a<+15; a++){
            CustomInvite one = new CustomInvite("Jane Doe","+2541234567", "imageurl");
            fetch.add(one);
        }
    }
}
