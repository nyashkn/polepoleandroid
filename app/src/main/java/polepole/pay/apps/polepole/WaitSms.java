package polepole.pay.apps.polepole;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.Toast;

import service.OtpService;

/**
 * Created by kenneth on 12/13/15.
 */
public class WaitSms extends AppCompatActivity{

    Toolbar mToolbar;
    EditText inputOtp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wait_sms);
        setTitle("Verify Number");
        inputOtp = (EditText) findViewById(R.id.inputOtp);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    /**
     * sending the OTP to server and activating the user
     */
    private void verifyOtp() {
        String otp = inputOtp.getText().toString().trim();

        if (!(otp.length()<=0)) {
            Intent grapprIntent = new Intent(getApplicationContext(), OtpService.class);
            grapprIntent.putExtra("otp", otp);
            startService(grapprIntent);
        } else {
            Toast.makeText(getApplicationContext(), "Please enter the OTP", Toast.LENGTH_SHORT).show();
        }
    }


}
