package polepole.pay.apps.polepole;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by kenneth on 12/14/15.
 */
public class ReportsAdapter extends ArrayAdapter<CustomReports> {

    private ArrayList<CustomReports> entries;
    private Activity activity;

    public ReportsAdapter(Activity a, int textViewResourceId, ArrayList<CustomReports> entries) {
        super(a, textViewResourceId, entries);
        this.entries = entries;
        this.activity = a;
    }

    public static class ViewHolder{
        public TextView amount;
        public TextView vendor;
        public TextView date;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            LayoutInflater vi =
                    (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.report_layout, null);
            holder = new ViewHolder();
            holder.amount = (TextView) v.findViewById(R.id.txtAmount);
            holder.vendor = (TextView) v.findViewById(R.id.txtVendor);
            holder.date = (TextView)v.findViewById(R.id.txtDate);

            v.setTag(holder);
        }
        else
            holder=(ViewHolder)v.getTag();

        final CustomReports custom = entries.get(position);
        if(custom != null){
            holder.amount.setText(custom.getAmount());
            holder.vendor.setText(custom.getVendor());
            holder.date.setText(custom.getDate());
        }

        return v;
    }
}
