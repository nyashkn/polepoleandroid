package polepole.pay.apps.polepole;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import helpers.PrefManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private PrefManager pref;


    Button btnSignUp;
    Button btnLogin;

    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("PolePole");

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        btnSignUp = (Button)findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(this);

        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        pref = new PrefManager(this);
        // Checking for user session
        // if user is already logged in, take him to main activity
        if (pref.isLoggedIn()) {
            Intent intent = new Intent(MainActivity.this, Tabs.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return;
        }

        if (pref.isWaitingForSms()) {
            Intent tabs = new Intent(this, WaitSms.class);
            startActivity(tabs);
            finish();
            return;
        }
        Intent register = new Intent(this, SignUp.class);
        startActivity(register);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onClick(View v) {
//        if(v.getId()==R.id.btnSignUp) {
//            Intent register = new Intent(this, SignUp.class);
//            startActivity(register);
//        }
//        if(v.getId()==R.id.btnLogin){
//            Intent tabs = new Intent(this, Tabs.class);
//            startActivity(tabs);
//        }
    }
}
