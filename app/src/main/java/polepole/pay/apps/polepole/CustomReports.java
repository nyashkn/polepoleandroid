package polepole.pay.apps.polepole;

/**
 * Created by kenneth on 12/14/15.
 */
public class CustomReports {

    String amount;
    String vendor;
    String date;

    public CustomReports(String amount, String vendor, String date){
        this.amount = amount;
        this.vendor = vendor;
        this.date = date;
    }

    public String getAmount() { return amount; }
    public void setAmount(String amount) { this.amount = amount; }

    public String getVendor() { return vendor; }
    public void setVendor(String vendor) { this.vendor = vendor; }

    public String getDate() { return date; }
    public void setDate(String date) { this.date = date; }

}
