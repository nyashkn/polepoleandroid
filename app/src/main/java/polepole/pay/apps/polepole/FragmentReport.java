package polepole.pay.apps.polepole;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by kenneth on 12/13/15.
 */
public class FragmentReport extends Fragment {

    ReportsAdapter listAdapter;
    ListView myList;

    private ArrayList<CustomReports> fetch = new ArrayList<CustomReports>();

    public FragmentReport() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_report, container, false);

        //The listview of contacts
        myList = (ListView) v.findViewById(R.id.listReports);
        // get the listview
        //expListView = (ExpandableListView)rootView.findViewById(R.id.lvExp);

        // preparing list data
        prepareListData();
        System.out.println("Prepare month has been run");

        listAdapter = new ReportsAdapter(getActivity(), R.id.listReports, fetch);

        // setting list adapter
        myList.setAdapter(listAdapter);

        return v;
    }

    public void prepareListData(){
        //Add your data here in a loop or whatever. I will use dummy data
        for(int a = 0; a<+15; a++){
            CustomReports one = new CustomReports("KSH 2500","Cakes and Cakes", "14th Dec, 2015");
            fetch.add(one);
        }
    }
}
