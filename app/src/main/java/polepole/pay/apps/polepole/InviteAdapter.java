package polepole.pay.apps.polepole;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by kenneth on 12/14/15.
 */
public class InviteAdapter extends ArrayAdapter<CustomInvite> {

    private ArrayList<CustomInvite> entries;
    private Activity activity;

    public InviteAdapter(Activity a, int textViewResourceId, ArrayList<CustomInvite> entries) {
        super(a, textViewResourceId, entries);
        this.entries = entries;
        this.activity = a;
    }

    public static class ViewHolder{
        public TextView name;
        public TextView number;
        public ImageView image;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            LayoutInflater vi =
                    (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.invite_layout, null);
            holder = new ViewHolder();
            holder.name = (TextView) v.findViewById(R.id.txtName);
            holder.number = (TextView) v.findViewById(R.id.txtPhone);
            holder.image = (ImageView)v.findViewById(R.id.imgFace);

            v.setTag(holder);
        }
        else
            holder=(ViewHolder)v.getTag();

        final CustomInvite custom = entries.get(position);
        if(custom != null){
            holder.name.setText(custom.getName());
            holder.number.setText(custom.getNumber());

            //you can set the image face here
        }

        return v;
    }
}
