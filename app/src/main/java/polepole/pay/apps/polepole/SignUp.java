package polepole.pay.apps.polepole;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import connect.Config;
import helpers.PrefManager;

/**
 * Created by kenneth on 12/13/15.
 */
@SuppressWarnings("deprecation")
public class SignUp extends AppCompatActivity implements View.OnClickListener{
    private static String TAG = SignUp.class.getSimpleName();

    Toolbar mToolbar;
    EditText fName, lName, mobileNumber, idNumber, firstPin, confirmPin;
    TextView terms;
    CheckBox acceptTerms;
    Button btnSignup;
    private ProgressBar progressBar;
    private PrefManager pref;

    //for GCM
    String projectNumber = "91957641202";
    GoogleCloudMessaging gcm;
    String registration_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setTitle("Register");

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        fName = (EditText) findViewById(R.id.enterFName);
        lName = (EditText) findViewById(R.id.enterLName);
        mobileNumber = (EditText) findViewById(R.id.enterPhone);
        idNumber = (EditText) findViewById(R.id.enterID);
        firstPin = (EditText) findViewById(R.id.enterPin);
        confirmPin = (EditText) findViewById(R.id.enterCPin);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        terms = (TextView) findViewById(R.id.txtTerms);
        acceptTerms = (CheckBox) findViewById(R.id.checkBox);
        btnSignup = (Button) findViewById(R.id.btnSignUp);
        pref = new PrefManager(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnSignup.setOnClickListener(this);
        terms.setOnClickListener(this);

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSignUp:
                validateForm();
                break;
            case R.id.txtTerms:
                termsDialog();
                break;
        }
    }

    public void termsDialog(){
        AlertDialog.Builder myDialog = new AlertDialog.Builder(SignUp.this);
        View view = LayoutInflater.from(SignUp.this).inflate(R.layout.terms_dialog_layout, null);
        myDialog.setView(view);
        myDialog.setTitle("Terms & Conditions");
//        myDialog.setPositiveButton("Back", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                //add your logic code here
//            }
//        });
        myDialog.setNegativeButton("Back", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        final AlertDialog alertDialog = myDialog.create();

        TextView message = (TextView)view.findViewById(R.id.txtTermsDialog);

        message.setText("Interest Charges: 4% - 7% per month depending on your credit score\n" +
                        "Default: (Penalties & Listing to CRB)\n" +
                        "PolePole will score you off your mobile usage & transaction history on your MPESA"
        );

        alertDialog.show();
    }

    private void validateForm() {
        String name = fName.getText().toString().trim() + " " + lName.getText().toString().trim();
        String idNo = idNumber.getText().toString().trim();
        String mobile = mobileNumber.getText().toString().trim();
        String pin = firstPin.getText().toString().trim();
        String pinConfirm = confirmPin.getText().toString().trim();
        Boolean valid = true;
        String errorResponseMessage = "";

        // validating empty name and email
        if (name.length() <  2 || idNo.length() == 0) {
            errorResponseMessage = "Please enter your details";
            valid = false;
        }

        if (!acceptTerms.isChecked()) {
            errorResponseMessage+="\n Please accept the terms by clicking on the check box";
            valid = false;
        }
        // validating mobile number
        // it should be of 10 digits length
        if (!isValidPhoneNumber(mobile)){
            errorResponseMessage+="\n Please enter valid mobile number i.e. 0722123456";
            valid = false;
        }

        if (!pinConfirm.contentEquals(pin)){
            errorResponseMessage+="\n The PIN number's are not the same";
            valid = false;
        }

        if (valid) {
            // request for sms
            progressBar.setVisibility(View.VISIBLE);
            btnSignup.setEnabled(false);
            // saving the mobile number in shared preferences
            pref.setMobileNumber(mobile);

            //create GCM ID for user
            registerGCM();
            // requesting for sms
            requestForSMS(name, idNo, mobile, pin, registration_id);
        } else {
            Toast.makeText(getApplicationContext(), errorResponseMessage, Toast.LENGTH_LONG).show();
        }
    }

    private void registerGCM() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                return null;
            }

            protected void doInBackground(Void... params) {
                try{
                    gcm = GoogleCloudMessaging.getInstance(getBaseContext());
                    registration_id = gcm.register(projectNumber);
                    Log.d("GcmIntentService", "Device registered, registration ID=" + registration_id);
                }
                catch(Exception r){
                    r.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }
    /**
     * Regex to validate the mobile number
     * mobile number should be of 10 digits length
     *
     * @param mobile
     * @return
     */
    private static boolean isValidPhoneNumber(String mobile) {
        String regEx = "^07[0-9]{8}$";
        return mobile.matches(regEx);
    }
    private void requestForSMS(final String name, final String idNo, final String mobile, final String pin, final String gcmID) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_REQUEST_SMS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONObject responseObj = new JSONObject(response);

                    // Parsing json object response
                    // response will be a json object
                    boolean error = responseObj.getBoolean("error");
                    String message = responseObj.getString("message");

                    // checking for error, if not error SMS is initiated
                    // device should receive it shortly
                    if (!error) {
                        // boolean flag saying device is waiting for sms
                        pref.setIsWaitingForSms(true);

                        // moving the screen to next pager item i.e otp screen
                        Intent waitSms = new Intent(getApplicationContext(), WaitSms.class);
                        startActivity(waitSms);
                        finish();
                        Toast.makeText(SignUp.this, message, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(SignUp.this,
                                "Error: " + message,
                                Toast.LENGTH_LONG).show();
                        btnSignup.setEnabled(true);
                    }

                    // hiding the progress bar
                    progressBar.setVisibility(View.GONE);

                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                    btnSignup.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(SignUp.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                btnSignup.setEnabled(true);
            }
        }) {

            /**
             * Passing user parameters to our server
             * @return
             */
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("id_number", idNo);
                params.put("mobile", mobile);
                params.put("pin", pin);
                //params.put("gcmID", gcmID);


                return params;
            }

        };

        // Adding request to request queue
        PoleApplication.getInstance().addToRequestQueue(strReq);
    }

}
