package polepole.pay.apps.polepole;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by kenneth on 12/13/15.
 */
public class FragmentPay extends Fragment implements View.OnClickListener{

    Button btnPayOne;
    Button btnPayTwo;
    Button btnPayThree;

    EditText enterTill;
    EditText enterAmount;

    public FragmentPay() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_pay, container, false);

        btnPayOne = (Button)v.findViewById(R.id.btnOneMonth);
        btnPayOne.setOnClickListener(this);

        btnPayTwo = (Button)v.findViewById(R.id.btnTwoMonths);
        btnPayTwo.setOnClickListener(this);

        btnPayThree = (Button)v.findViewById(R.id.btnThreeMonths);
        btnPayThree.setOnClickListener(this);

        enterTill = (EditText)v.findViewById(R.id.enterBGNumber);
        enterAmount = (EditText)v.findViewById(R.id.enterAmount);


        return v;
    }

    @Override
    public void onClick(View v) {
        //add your button click code here
        if(v.getId()==R.id.btnOneMonth){
            if(enterAmount.getText().toString().length()>0 && enterTill.getText().toString().length()>0){
                confirmDialog("One month",enterTill.getText().toString(), enterAmount.getText().toString());
            }
            else{
                Toast.makeText(getActivity(), "Please fill in a till number and amount", Toast.LENGTH_SHORT).show();
            }
        }
        //do the same for the other buttons
    }

    public void confirmDialog(String duration, String till, String amount){
        AlertDialog.Builder myDialog = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.payment_dialog_layout, null);
        myDialog.setView(view);
        myDialog.setTitle("Confirm payment");
        myDialog.setPositiveButton("Pay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //add your logic code here
            }
        });
        myDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        final AlertDialog alertDialog = myDialog.create();

        TextView message = (TextView)view.findViewById(R.id.txtMessage);
        if(duration.equals("One month")){
            message.setText("Please confirm that you will pay one month later to the following account:" +
                    "\nTill/Buy goods number: "+till+"\nAmount: "+amount);
        }
        if(duration.equals("Two months")){
            message.setText("Please confirm that you will pay two month later to the following account:" +
                    "\nTill/Buy goods number: "+till+"\nAmount: "+amount);
        }
        if(duration.equals("Three months")){
            message.setText("Please confirm that you will pay three month later to the following account:" +
                    "\nTill/Buy goods number: "+till+"\nAmount: "+amount);
        }
        alertDialog.show();
    }
}
