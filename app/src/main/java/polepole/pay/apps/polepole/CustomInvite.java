package polepole.pay.apps.polepole;

/**
 * Created by kenneth on 12/14/15.
 */
public class CustomInvite {

    String name;
    String number;
    String image;

    public CustomInvite(String name, String number, String image){
        this.name = name;
        this.number = number;
        this.image = image;
    }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getNumber() { return number; }
    public void setNumber(String number) { this.number = number; }

    public String getImage() { return image; }
    public void setImage(String image) { this.image = image; }

}
